import os

from pytest import mark

from torxtools import pathtools


@mark.parametrize(
    "datain, dataout",
    [
        # fmt: off
        (["~/$SPAM/one", "~/$SPAM/two", None], ["/home/jdoe/eggs/one", "/home/jdoe/eggs/two", None]),
        ([], []),
        (None, None),
        ("$XDG_CACHE_HOME/eggs", "/home/jdoe/.cache/eggs"),
        ("$SUBHOME/eggs", "$HOME/sub/eggs"),
        # fmt: on
    ],
)
def test_pathtools_expandpath(_noenv, datain, dataout):
    assert pathtools.expandpath(datain) == dataout


def test_pathtools_find_pyproject(_noenv):
    # Absolute path
    expected = os.path.dirname(__file__) + "/../pyproject.toml"
    expected = os.path.normpath(expected)
    assert pathtools.find_pyproject(os.path.dirname(__file__)) == expected

    # Relative path
    assert pathtools.find_pyproject(".") == expected
