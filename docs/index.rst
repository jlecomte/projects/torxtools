.. meta::
   :robots: noindex, follow

torxtools
=========

Less generic functionalities than Phillips and Pozidriv tools. One size fits most. MacGyver's French army spork.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   introduction.rst
   argtools.rst
   cfgtools.rst
   ctxtools.rst
   dicttools.rst
   pathtools.rst
   testtools.rst
   xdgtools.rst


.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   license.rst

Indices and tables
==================

* :ref:`genindex`
